<?php
/**
 * Register Custom Post Type : Competition
 */
function create_posttype_competition() {

	$labels = array(
		'name'                  => _x( 'Competitions', 'Competitions', 'twentynineteenchild' ),
		'singular_name'         => _x( 'competition', 'Competition', 'twentynineteenchild' ),
		'menu_name'             => __( 'Competitions', 'twentynineteenchild' ),
		'name_admin_bar'        => __( 'Competitions', 'twentynineteenchild' ),
		'archives'              => __( 'Item Archives', 'twentynineteenchild' ),
		'attributes'            => __( 'Item Attributes', 'twentynineteenchild' ),
		'parent_item_colon'     => __( 'Parent Item:', 'twentynineteenchild' ),
		'all_items'             => __( 'All Competitions', 'twentynineteenchild' ),
		'add_new_item'          => __( 'Add New Competition', 'twentynineteenchild' ),
		'add_new'               => __( 'Add New Competition', 'twentynineteenchild' ),
		'new_item'              => __( 'New Competition', 'twentynineteenchild' ),
		'edit_item'             => __( 'Edit Competition', 'twentynineteenchild' ),
		'update_item'           => __( 'Update Competition', 'twentynineteenchild' ),
		'view_item'             => __( 'View Competition', 'twentynineteenchild' ),
		'view_items'            => __( 'View Competitions', 'twentynineteenchild' ),
		'search_items'          => __( 'Search Competition', 'twentynineteenchild' ),
		'not_found'             => __( 'Not found', 'twentynineteenchild' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'twentynineteenchild' ),
		'featured_image'        => __( 'Featured Image', 'twentynineteenchild' ),
		'set_featured_image'    => __( 'Set featured image', 'twentynineteenchild' ),
		'remove_featured_image' => __( 'Remove featured image', 'twentynineteenchild' ),
		'use_featured_image'    => __( 'Use as featured image', 'twentynineteenchild' ),
		'insert_into_item'      => __( 'Insert into competition', 'twentynineteenchild' ),
		'uploaded_to_this_item' => __( 'Uploaded to this competition', 'twentynineteenchild' ),
		'items_list'            => __( 'competitions list', 'twentynineteenchild' ),
		'items_list_navigation' => __( 'competitions list navigation', 'twentynineteenchild' ),
		'filter_items_list'     => __( 'Filter competitions list', 'twentynineteenchild' ),
	);
	$args = array(
		'label'                 => __( 'competition', 'twentynineteenchild' ),
		'description'           => __( 'This post hold Competition information.', 'twentynineteenchild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'query_var'				=> true,
		'rewrite'				=> array( 'slug' => 'competitions' ),
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'competition', $args );

}
add_action( 'init', 'create_posttype_competition', 0 );

/**
 * Add custom field to Competition Post Type
 */
function twentynineteenchild_competition_metabox() {
	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_demo = new_cmb2_box( array(
		'id'            => CHILD_THEME_PREFIX.'competition_metabox',
		'title'         => esc_html__( 'Competition Information', 'twentynineteenchild' ),
		'object_types'  => array( 'competition' ), 
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Start Date', 'twentynineteenchild' ),
		'desc' => esc_html__( 'Enter competition start date', 'twentynineteenchild' ),
		'id'   => CHILD_THEME_PREFIX.'competition_start_date',
		'type' => 'text_date',
		// 'date_format' => 'Y-m-d',
		'date_format' => 'd-M-Y',
		//'date_format' => 'j/n/Y',
		'default'     => date( 'd-M-Y' ),
		'attributes' => array(
			'required' => 'required',
			'data-datepicker' => json_encode( array(
				'defaultDate' => null
			)),
		),
		'classes'    => array( 'required' ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'End Date', 'twentynineteenchild' ),
		'desc' => esc_html__( 'Enter competition end date', 'twentynineteenchild' ),
		'id'   => CHILD_THEME_PREFIX.'competition_end_date',
		'type' => 'text_date',
		// 'date_format' => 'Y-m-d',
		'date_format' => 'd-M-Y',
		//'date_format' => 'j/n/Y',
		'default'     => date( 'd-M-Y' ),
		'attributes' => array(
			'required' => 'required',
			'data-datepicker' => json_encode( array(
				'defaultDate' => null
			)),
		),
		'classes'    => array( 'required' ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Banner Image', 'twentynineteenchild' ),
		'desc' => esc_html__( 'Upload banner image or enter a URL for competition.', 'twentynineteenchild' ),
		'id'   => CHILD_THEME_PREFIX.'competition_banner_image',
		'type' => 'file',
		'attributes' => array(
			'required' => 'required',
		),
		'classes'    => array( 'required' ),
	) );
}
add_action( 'cmb2_admin_init', 'twentynineteenchild_competition_metabox' );