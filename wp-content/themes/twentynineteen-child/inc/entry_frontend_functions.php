<?php
/**
 * Get the url of templalte
 * @param  [string] $template_name [template name]
 * @return [url]                [url will be return]
 */
function get_template_url($template_name)
{
	$pages = get_posts([
        'post_type' => 'page',
        'post_status' => 'publish',
        'meta_query' => [
            [
                'key' => '_wp_page_template',
                'value' => $template_name.'.php',
                'compare' => '='
            ]
        ]
    ]);
    if(!empty($pages))
    {
      foreach($pages as $pages__value)
      {
          return get_permalink($pages__value->ID);
      }
    }
    return get_bloginfo('url');
}

/**
 * to display entry form url for every competition post
 */
if(!function_exists('twentynineteenchild_entryform_button')){
	function twentynineteenchild_entryform_button(){
		$entry_form_url = get_template_url('template-submit-entry');
    $competition_id = get_the_ID();
    ?>
		<a href="<?php echo home_url();?>/competition/submit-entry/<?php echo get_the_ID();?>" class="button button-primary button-large" id="submit_entry"><?php _e( 'Submit Entry', 'twentynineteenchild' ); ?></a>
		<?php
	}
}

/**
 * ajax part
 */
function ajax_entry_submit_fn() {
    check_ajax_referer( 'secure_entry', 'security' );

    // The $_REQUEST contains all the data sent via ajax
    if ( isset($_REQUEST) ) {

      // insert entry post
      $entry_post = array(
        'post_type'    => 'entries',
        'post_title'    => wp_strip_all_tags( $_REQUEST['first_name'] . $_REQUEST['last_name'] ),
        'post_content'  => $_REQUEST['description'],
        'post_status'   => 'publish',
        'post_author'   => 1,
      );

      // Insert the post into the database
      $entry_id = wp_insert_post( $entry_post );

      if ($entry_id) {
         // insert post meta
         add_post_meta($entry_id, 'twentynineteenchild_entry_first_name', $_REQUEST['first_name']);
         add_post_meta($entry_id, 'twentynineteenchild_entry_last_name', $_REQUEST['last_name']);
         add_post_meta($entry_id, 'twentynineteenchild_entry_email', $_REQUEST['email']);
         add_post_meta($entry_id, 'twentynineteenchild_entry_phone', $_REQUEST['phone']);
         add_post_meta($entry_id, 'twentynineteenchild_entry_competition_id', $_REQUEST['cid']);
      }
       
      // Insert the post into the database
      wp_insert_post( $my_post );
     
      echo $entry_id;   
        // If you're debugging, it might be useful to see what was sent in the $_REQUEST
        // print_r($_REQUEST);
     
    }
     
    // Always die in functions echoing ajax content
   die();
}
 
add_action( 'wp_ajax_ajax_entry_submit', 'ajax_entry_submit_fn' );
// If you wanted to also use the function for non-logged in users (in a theme for example)
add_action( 'wp_ajax_nopriv_ajax_entry_submit', 'ajax_entry_submit_fn' );

function example_ajax_enqueue() {
  
  // Enqueue javascript on the frontend.
  wp_enqueue_script( 'example-ajax-script', get_stylesheet_directory_uri() . '/js/entry-submit.js', array('jquery') );
  
  // The wp_localize_script allows us to output the ajax_url path for our script to use.
  wp_localize_script( 'example-ajax-script', 'example_ajax_obj', array(
    'ajax_url' => admin_url( 'admin-ajax.php' ),
    'ajax_nonce' => wp_create_nonce('secure_entry'),
    'success_msg' =>  __( 'Entry added successfully.', 'twentynineteenchild' ),
    'fail_msg' => __( 'Failed to add Entry.', 'twentynineteenchild' ),
    'ajax_loading_img' => get_stylesheet_directory_uri() . '/images/ajax-loader.gif',
  ) );

}
add_action( 'wp_enqueue_scripts', 'example_ajax_enqueue' );