<?php
/**
 * Register Custom Post Type : Entry
 */
function create_posttype_entries() {

	$labels = array(
		'name'                  => _x( 'Entries', 'Entries', 'twentynineteenchild' ),
		'singular_name'         => _x( 'Entry', 'Entry', 'twentynineteenchild' ),
		'menu_name'             => __( 'Entries', 'twentynineteenchild' ),
		'name_admin_bar'        => __( 'Entries', 'twentynineteenchild' ),
		'archives'              => __( 'Item Archives', 'twentynineteenchild' ),
		'attributes'            => __( 'Item Attributes', 'twentynineteenchild' ),
		'parent_item_colon'     => __( 'Parent Item:', 'twentynineteenchild' ),
		'all_items'             => __( 'All Entries', 'twentynineteenchild' ),
		'add_new_item'          => __( 'Add New Entry', 'twentynineteenchild' ),
		'add_new'               => __( 'Add New Entry', 'twentynineteenchild' ),
		'new_item'              => __( 'New Entry', 'twentynineteenchild' ),
		'edit_item'             => __( 'Edit Entry', 'twentynineteenchild' ),
		'update_item'           => __( 'Update Entry', 'twentynineteenchild' ),
		'view_item'             => __( 'View Entry', 'twentynineteenchild' ),
		'view_items'            => __( 'View Entries', 'twentynineteenchild' ),
		'search_items'          => __( 'Search Entry', 'twentynineteenchild' ),
		'not_found'             => __( 'Not found', 'twentynineteenchild' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'twentynineteenchild' ),
		'featured_image'        => __( 'Featured Image', 'twentynineteenchild' ),
		'set_featured_image'    => __( 'Set featured image', 'twentynineteenchild' ),
		'remove_featured_image' => __( 'Remove featured image', 'twentynineteenchild' ),
		'use_featured_image'    => __( 'Use as featured image', 'twentynineteenchild' ),
		'insert_into_item'      => __( 'Insert into entry', 'twentynineteenchild' ),
		'uploaded_to_this_item' => __( 'Uploaded to this entry', 'twentynineteenchild' ),
		'items_list'            => __( 'entries list', 'twentynineteenchild' ),
		'items_list_navigation' => __( 'entries list navigation', 'twentynineteenchild' ),
		'filter_items_list'     => __( 'Filter entries list', 'twentynineteenchild' ),
	);
	$args = array(
		'label'                 => __( 'Entry', 'twentynineteenchild' ),
		'description'           => __( 'This post hold Emtry information.', 'twentynineteenchild' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => false,
	);
	register_post_type( 'entries', $args );

}
add_action( 'init', 'create_posttype_entries', 0 );

/**
 * set title for custom fiel at listing
 */
add_filter('manage_entries_posts_columns', 'show_first_last_name');
function show_first_last_name( $defaults ) {
	$defaults['title']  = 'Title';
	unset($defaults['date']);
    $defaults['email']    = 'Email';
    $defaults['phone']    = 'Phone';
    $defaults['competition']    = 'Competition';
    $defaults['date']   = 'Date';

    return $defaults;
}

/**
 * set data for custom field title
 */
add_action( 'manage_entries_posts_custom_column', 'bs_event_table_content', 10, 2 );
function bs_event_table_content( $column_name, $post_id ) {
    if ($column_name == 'email') {
    	$email = get_post_meta($post_id, 'twentynineteenchild_entry_email', true);
    	if($email != '')
    		echo $email;
    }
    if ($column_name == 'phone') {
    	$phone = get_post_meta($post_id, 'twentynineteenchild_entry_phone', true);
    	if($phone != '')
    		echo $phone;
    }
    if ($column_name == 'competition') {
    	$competition = get_post_meta($post_id, 'twentynineteenchild_entry_competition_id', true);
    	if($competition != '')
    		//echo get_edit_post_link( $competition );
    		echo '<a href="'.get_edit_post_link( $competition ).'">'.get_the_title($competition).'</a>';
    }
    
    
}

/**
 * reorder the title and fields.
 */
add_filter( 'manage_entries-review_sortable_columns', 'bs_event_table_sorting' );
function bs_event_table_sorting( $columns ) {

  $columns['names']   = 'Overall Ratings';
  return $columns;
}

/**
 * Add custom field to Entry Post Type
 */
function twentynineteenchild_entry_metabox() {
	$cmb_demo = new_cmb2_box( array(
		'id'            => CHILD_THEME_PREFIX.'entry_metabox',
		'title'         => esc_html__( 'Entry Information', 'twentynineteenchild' ),
		'object_types'  => array( 'entries' ), 
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'First Name', 'twentynineteenchild' ),
		'desc' => esc_html__( 'Enter First Name', 'twentynineteenchild' ),
		'id'   => CHILD_THEME_PREFIX.'entry_first_name',
		'type' => 'text_medium',
		'attributes' => array(
			'required' => 'required',
			'data-datepicker' => json_encode( array(
				'defaultDate' => null
			)),
		),
		'classes'    => array( 'required' ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Last Name', 'twentynineteenchild' ),
		'desc' => esc_html__( 'Enter Last Name', 'twentynineteenchild' ),
		'id'   => CHILD_THEME_PREFIX.'entry_last_name',
		'type' => 'text_medium',
		'attributes' => array(
			'required' => 'required',
			'data-datepicker' => json_encode( array(
				'defaultDate' => null
			)),
		),
		'classes'    => array( 'required' ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Email', 'twentynineteenchild' ),
		'desc' => esc_html__( 'Enter email id', 'twentynineteenchild' ),
		'id'   => CHILD_THEME_PREFIX.'entry_email',
		'type' => 'text_email',
		'attributes' => array(
			'required' => 'required',
			'data-datepicker' => json_encode( array(
				'defaultDate' => null
			)),
		),
		'classes'    => array( 'required' ),
	) );

	$cmb_demo->add_field( array(
		'name' => esc_html__( 'Phone', 'twentynineteenchild' ),
		'desc' => esc_html__( 'Enter Phone Number', 'twentynineteenchild' ),
		'id'   => CHILD_THEME_PREFIX.'entry_phone',
		'type' => 'text_medium',
		'attributes' => array(
			'required' => 'required',
			'data-datepicker' => json_encode( array(
				'defaultDate' => null
			)),
		),
		'classes'    => array( 'required' ),
	) );

	$cmb_demo->add_field(array(
        'name' => __('Select competition', 'cmb2'),
        'id' => CHILD_THEME_PREFIX.'entry_competition_id',
        'type' => 'select',
        'show_option_none' => true,
        'attributes' => array(
			'required' => 'required',
			'data-datepicker' => json_encode( array(
				'defaultDate' => null
			)),
		),
		'classes'    => array( 'required' ),
        // Use an options callback
        'options_cb' => 'show_cat_or_dog_options',
    ));

	//its options to get competition page name and ids
    function show_cat_or_dog_options() {
        // return a standard options array
        // query for your post type
        $post_type_query  = new WP_Query(  
            array (  
                'post_type'      => 'competition',  
                'posts_per_page' => -1  
            )  
        ); 

        
        $the_query = new WP_Query( array(
            'posts_per_page'   => -1,
            'post_type'        => 'competition',
        ) );
        $posts = $the_query->posts;
        // the key equals the ID, the value is the post_title
        $competition_arr = wp_list_pluck( $posts, 'post_title', 'ID' );
        return $competition_arr;
    }

	
}
add_action( 'cmb2_admin_init', 'twentynineteenchild_entry_metabox' );