jQuery(document).ready(function($) {
    // Get the form.
    var form = jQuery('#submitentryform');

    jQuery("#submitentryform").on("click",".submit_entry", function(){
      

    });

    jQuery(form).submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();

        //append loading icon  
        jQuery('#ajaxOutput').html("<img src="+example_ajax_obj.ajax_loading_img+">");
        
        

        var formData = jQuery(form).serialize()
        // Serialize the form data.
        formData += '&action=ajax_entry_submit&security='+example_ajax_obj.ajax_nonce;
        // This does the ajax request
        $.ajax({
            url: example_ajax_obj.ajax_url, // or example_ajax_obj.ajaxurl if using on frontend
            data: formData,
            success:function(data) {
                // This outputs the result of the ajax request
                if(data > 0){
                  jQuery('#ajaxOutput').html(example_ajax_obj.success_msg);
                  resetEntryForm();
                }
            },
            error: function(errorThrown){
              jQuery('#ajaxOutput').html("<img src="+example_ajax_obj.fail_msg+">");
              resetEntryForm();
            }
        });  

    });
              
});



function resetEntryForm(){
    jQuery('#submitentryform').trigger("reset");
    setTimeout( function()  { jQuery('#ajaxOutput').html(''); }, 5000);
    
}