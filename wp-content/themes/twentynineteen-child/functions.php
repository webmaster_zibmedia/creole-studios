<?php
/**
 * Twentynineteen-child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package twentynineteen-child
 */

add_action( 'wp_enqueue_scripts', 'twentynineteen_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function twentynineteen_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'twentynineteen-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'twentynineteen-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'twentynineteen-style' )
	);

}

/**
 * Defined prefix for every custom field to make it unique
 * We will use it for CMB2 here
 */
define(CHILD_THEME_PREFIX, 'twentynineteenchild_');

/**
  * Set up Child Theme's textdomain.
  *
  * Declare textdomain for this child theme.
  * Translations can be added to the /languages/ directory.
  */
function twentynineteenchild_theme_setup() {
    load_child_theme_textdomain( 'twentynineteenchild', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'twentynineteenchild_theme_setup' );

/**
 * Include CMB2 framework
 * Included for custom fields
 */
if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

/**
 * Competition related functions : 
 * Register Custom Post Types
 * Register Meta Box
 */
if ( file_exists( dirname( __FILE__ ) . '/inc/competition_post_type_info.php' ) ) {
	require_once dirname( __FILE__ ) . '/inc/competition_post_type_info.php';
}

/**
 * Entry related functions : 
 * Register Custom Post Types
 * Register Meta Box
 */
if ( file_exists( dirname( __FILE__ ) . '/inc/entry_post_type_info.php' ) ) {
	require_once dirname( __FILE__ ) . '/inc/entry_post_type_info.php';
}

/**
 * Frontend functions : 
 * Display Button
 * Ajax submit and store
 */
if ( file_exists( dirname( __FILE__ ) . '/inc/entry_frontend_functions.php' ) ) {
  require_once dirname( __FILE__ ) . '/inc/entry_frontend_functions.php';
}


function rewrite_competition_url(){
	add_rewrite_rule('^competition/([^/]*)/([^/]*)/?', 'index.php?page_id=91&submit-entry=$matches[1]&cid=$matches[2]','top');
}

function register_custom_query_vars($vars){
	array_push($vars, 'submit-entry');
	array_push($vars, 'cid');
	return $vars;
}
add_action('init','rewrite_competition_url');
add_action('query_vars','register_custom_query_vars',1);