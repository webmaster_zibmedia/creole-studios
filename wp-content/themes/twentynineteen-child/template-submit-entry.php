<?php
/**
 * Template Name: Entry Form
 * The template for displaying submit entry form
 *
 * @link https://developer.wordpress.org/themes/template-files-section/page-template-files/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

$submit_entry = get_query_var('submit-entry');
$cid = get_query_var('cid');

get_header();


?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php


			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

			endwhile; // End of the loop.
			?>

			<!-- submit form -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="entry-content">

					<form action="javascript:void(0);" method="POST" id="submitentryform">
						<fieldset>
							<legend>Entry Form</legend>
							<p>
								<label for="first_name">
									<?php _e( 'First Name:', 'twentynineteenchild' ); ?>
								</label>
								<input type="text" size="30" name="first_name" id="first_name" required>
							</p>

							<p>
								<label for="last_name">
									<?php _e( 'Last Name:', 'twentynineteenchild' ); ?>
								</label>
								<input type="text" size="30" name="last_name" id="last_name" required>
							</p>

							<p>
								<label for="email">
									<?php _e( 'Email:', 'twentynineteenchild' ); ?>
								</label>
								<input type="email" size="30" name="email" id="email" required>
							</p>

							<p>
								<label for="phone">
									<?php _e( 'Phone:', 'twentynineteenchild' ); ?>
								</label>
								<input type="number" size="30" name="phone" id="phone" required>
							</p>
								<input type="hidden" size="30" name="cid" id="cid" value="<?= $cid;?>">
							<p>
								<label for="description">
									<?php _e( 'Description::', 'twentynineteenchild' ); ?>
								</label>
								<textarea name="description" id="description" required></textarea>
							</p>

							<p>
								<input type="submit" class="submit_entry" id="submit_entry">
							</p>
							<p>
								<div id="ajaxOutput"></div>
							</p>
						</fieldset>
					</form>

				</div>
			</article>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
