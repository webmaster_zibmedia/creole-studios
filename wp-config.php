<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress-demo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'NA%rteFttxIXR7FA|ASs{pTA`j+pl)@8NW![~VanJ;,}qyy?ke)%>g^Fn|8@0|Xu' );
define( 'SECURE_AUTH_KEY',   '%Q_ii>tuI#_1]}V#Ki;]v5]m6M%etLgZwf_NNDr(i=3RCl43tN)Nq.X|yIvMH_K-' );
define( 'LOGGED_IN_KEY',     'D8o.sgw1Sh):&%bF |!Kd]~5-HA</F)e*(!+4(rHIHbv|v*C~G7V>biYgurDwP@Z' );
define( 'NONCE_KEY',         'iU13@`VF+p9;,D%ZTRFrD(1ss{Ey2w<ivSl1sK;&V<^E*-L+w*w3]z9fS%MbJ6;A' );
define( 'AUTH_SALT',         'E*#&iY%SQ{~^ !mQ5YLF8#L:#^HG-1uK;~ed8C4~/CnrMFGITJA8-kTavC8l@uL:' );
define( 'SECURE_AUTH_SALT',  'A@VA;T@Y]yH0VNJGx^Mkx2S!yo;9`ehc[=wK~!j]LmGl:C;St9x]F._A@^CJMsr+' );
define( 'LOGGED_IN_SALT',    'S`z1/r ~ECvP<+z`]YHEXRYTnX_p6I0yhwA`p2a<:uVkWne^6#B?+p^Y%QQd|5M5' );
define( 'NONCE_SALT',        '%lS4e=w;+I%[d+EA)f[#FLf9}mZ]`mw}A`$Iaj@e+zPdC1e7 cvo]=9{B)Z;F>-<' );
define( 'WP_CACHE_KEY_SALT', 'e|fe>2]CqxjtSK[IH+j<2Pq`<,*&yVkM)`G#P6y(!YJ~rrg_>Eoxt|yv,Y^pc[2f' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
